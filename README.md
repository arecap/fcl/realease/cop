# arecap cop

Context Oriented Programming with Spring


Enable @ContextOriented Advice Bean declaration 
using contextualj-lang annotation for pointcut predicate
and an InverseMultiplexer Advice Join Points  


Spring Contexts Beans in @ContextOriented predicate are 
{@link org.arecap.contextualj.lang.weaving.InverseMultiplexer} IntroductionInterceptor
which use annotated interface expression pointcuts 
{@Link org.arecap.contextualj.lang.annotation.expression.Expression} 
to link @ContextOriented Advice Bean joinpoint execution;

## Types of ARECAP COP Advice 
  
1. Bid
2. Wrap  
3. Track
4. Handling Return
5. Handling Throwing
6. Final Binding