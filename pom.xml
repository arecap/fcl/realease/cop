<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.jcabi</groupId>
        <artifactId>parent</artifactId>
        <!-- check the latest version at http://parent.jcabi.com -->
        <version>0.49.9</version>
    </parent>

    <groupId>org.arecap</groupId>
    <artifactId>cop</artifactId>

    <properties>
        <revision>1.0.0-RELEASE</revision>
        <main.basedir>${basedir}</main.basedir>
        <java.version>1.8</java.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <spring.version>5.2.5.RELEASE</spring.version>
        <arecap.contextualj.lang.version>1.0.0-SNAPSHOT</arecap.contextualj.lang.version>

        <maven-release-plugin.version>2.5.2</maven-release-plugin.version>
        <maven-site-plugin.version>3.6</maven-site-plugin.version>
        <maven-compiler-plugin.version>3.7.0</maven-compiler-plugin.version>
        <maven-jar-plugin.version>3.0.2</maven-jar-plugin.version>
        <maven-javadoc-plugin.version>3.0.0-M1</maven-javadoc-plugin.version>
        <maven-source-plugin.version>3.0.1</maven-source-plugin.version>
        <maven-gpg-plugin.version>1.6</maven-gpg-plugin.version>
    </properties>

    <version>${revision}</version>
    <packaging>jar</packaging>

    <name>${project.groupId}:${project.artifactId}</name>
    <description>Context Oriented Programming with Spring</description>
    <url>
        https://gitlab.com/arecap/fcl/java/release/cop.git
    </url>
    <organization>
        <name>ARECAP</name>
        <url>https://arecap.org</url>
    </organization>
    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0</url>
        </license>
    </licenses>
    <developers>
        <developer>
            <id>gavianu</id>
            <name>Octavian Stirbei</name>
            <email>octavianstirbei at gmail.com</email>
            <organization>ARECAP</organization>
            <organizationUrl>https://arecap.org</organizationUrl>
            <roles>
                <role>Project lead</role>
            </roles>
        </developer>
        <developer>
            <id>georgetheboboc</id>
            <name>George Boboc</name>
            <email>boboc.george at gmail.com</email>
            <organization>ARECAP</organization>
            <organizationUrl>https://arecap.org</organizationUrl>
            <roles>
                <role>Developer</role>
            </roles>
        </developer>
    </developers>
    <scm>
        <connection>scm:git:git://gitlab.com/arecap/fcl/realease/cop.git</connection>
        <developerConnection>scm:git:ssh://git@gitlab.com/arecap/fcl/realease/cop.git</developerConnection>
        <url>https://gitlab.com/arecap/fcl/realease/cop</url>
    </scm>
    <dependencies>
        <!-- Compile -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.arecap</groupId>
            <artifactId>contextualj-lang</artifactId>
            <version>${arecap.contextualj.lang.version}</version>
        </dependency>
        <!-- Test -->
    </dependencies>
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>${maven-release-plugin.version}</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-site-plugin</artifactId>
                <version>${maven-site-plugin.version}</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler-plugin.version}</version>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                    <parameters>true</parameters>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>${maven-jar-plugin.version}</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                            <addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>${maven-javadoc-plugin.version}</version>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                        <inherited>true</inherited>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>${maven-source-plugin.version}</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-gpg-plugin</artifactId>
                <version>${maven-gpg-plugin.version}</version>
                <executions>
                    <execution>
                        <id>sign-artifacts</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>sign</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>