/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.cop;


import org.arecap.contextualj.lang.weaving.PointcutLinkSpecification;

import java.lang.reflect.AnnotatedElement;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 */
public final class ApplicationContextLink {

    public static boolean isLink(AnnotatedElement signal, AnnotatedElement link) {
        List<PointcutLinkSpecification> specifications = BeanUtil.getBeansOfType(PointcutLinkSpecification.class).entrySet()
                .stream().filter(p -> p.getValue().canHandle(link)).map(Map.Entry::getValue).collect(Collectors.toList());
        for(PointcutLinkSpecification specification: specifications) {
            if(!specification.isInverseMultiplexerLink(signal, link)) {
                return false;
            }
        }
        return specifications.size() > 0;
    }

}
