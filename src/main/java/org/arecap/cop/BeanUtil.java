/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.arecap.cop;


import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.lang.annotation.Annotation;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * Spring bean context shortcut access util
 *
 *
 * @author George Boboc
 * @author Octavian Stirbei
 *
 * @since 1.0.0
 */
@Service
@Order(value= Ordered.HIGHEST_PRECEDENCE)
public final class BeanUtil  implements ApplicationContextAware {

    /**
     *
     *
     * Application context strong reference
     *
     **/
     private static ApplicationContext context;


    /*
     *
     *
     *
     *
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    /*
     * Util method for scan bean for annotation
     *
     * @return Map<String, Class<?>> the bean components map names bean types in context metadata
     *
     */
    public static Map<String, Class<?>> getAnnotatedBeans(Class<? extends Annotation> annotationType) {
        String[] beanNames = BeanUtil.getBeanNamesForAnnotation(annotationType);
        Map<String, Class<?>> results = new LinkedHashMap<>(beanNames.length);
        for (String beanName : beanNames) {
            results.put(beanName, BeanUtil.getBeanType(beanName));
        }
        return results;
    }

    /*
     * Util method to use context components support in static method
     * where {@link org.springframework.beans.factory.annotation.Autowired} it can't be used
     *
     *  The Bean Context Scope Component
     *
     */
    public static Object getBean(Class<?> beanClass) {
        if (context == null) {
            //on init phase
            return null;
        }
        return context.getBean(beanClass);
    }

    /*
     *
     * Util method to use context components support in static method
     * where {@link org.springframework.beans.factory.annotation.Autowired} it can't be used
     *
     * @return
     *
     *  The Bean Context Scope Component
     *
     */
    public static Object getBean(String beanName) {
        if (context == null) {
            //on init phase
            return null;
        }
        return context.getBean(beanName);
    }

    /*
     *
     * Util method to use context components support in static method
     * where {@link org.springframework.beans.factory.annotation.Autowired} it can't be used
     *
     * @return
     *
     *  the bean component type
     *
     */
    public static Class<?> getBeanType(String beanName) {
        if (context == null) {
            //on init phase
            return null;
        }
        return context.getType(beanName);
    }

    /*
     *
     * Util method to use context components support in static method
     * where {@link org.springframework.beans.factory.annotation.Autowired} it can't be used
     *
     * @return
     *
     *  Map<String, Object> the bean components map by type  in context metadata
     *
     */
    public static <T> Map<String, T> getBeansOfType(Class<T> beanType) {
        if (context == null) {
            //on init phase
            return null;
        }
        return context.getBeansOfType(beanType);
    }

    /*
     *
     * Util method to use context components support in static method
     * where {@link org.springframework.beans.factory.annotation.Autowired} it can't be used
     *
     * @return
     *
     *  The request scope bean factory
     *
     */
    public static BeanFactory getBeanFactory() {
        if (context == null) {
            return null;
        }
        return context.getAutowireCapableBeanFactory();
    }

    /*
     *
     * Util method to use context components support in static method
     * where {@link org.springframework.beans.factory.annotation.Autowired} it can't be used
     *
     * @return
     *
     *  the bean names for annotation type
     *
     */
    public static String[] getBeanNamesForAnnotation(Class<? extends Annotation> annotation) {
        if (context == null) {
            return new String[0];
        }
        return context.getBeanNamesForAnnotation(annotation);
    }

}