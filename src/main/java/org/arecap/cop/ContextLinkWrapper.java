/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.cop;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.arecap.contextualj.lang.weaving.PointcutLinkSpecification;
import org.arecap.cop.annotation.ContextOriented;
import org.arecap.cop.proxy.InverseMultiplexerIntroductionInterceptor;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.aop.support.DefaultIntroductionAdvisor;
import org.springframework.aop.target.SingletonTargetSource;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 */
@Configuration
@ComponentScan("org.springframework.cop")
public class ContextLinkWrapper  implements BeanPostProcessor {

    /** Logger available to subclasses */
    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private BeanUtil beanUtil;

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if(bean != null) {
            if(PointcutLinkSpecification.class.isAssignableFrom(bean.getClass())) {
                return bean;
            }
            logger.debug("postProcess bean signal:\t"+beanName);
            List<Class<?>> links = getContextOrientedBeans(beanName).entrySet().stream()
                    .filter(p -> ApplicationContextLink.isLink(AopUtils.getTargetClass(bean), p.getValue()))
                    .map(p -> p.getValue())
                    .sorted(new AnnotationAwareOrderComparator()).collect(Collectors.toList());
            if(links.size() > 0) {
                logger.debug("bean signal:\t"+beanName+"\thas:\t"+links.size()+"\tlinks");
                return wrapBean(bean, beanName, links);
            }
        }
        return bean;
    }

    private Map<String, Class<?>> getContextOrientedBeans(String beanName) {
        String[] beanNames = BeanUtil.getBeanNamesForAnnotation(ContextOriented.class);
        Map<String, Class<?>> results = new LinkedHashMap<>(beanNames.length);
        for (String name : beanNames) {
            if(!name.equalsIgnoreCase(beanName)) {
                results.put(name, BeanUtil.getBeanType(name));
            }
        }
        return results;

    }

    private Object wrapBean(Object bean, String beanName, List<Class<?>> links) {
        logger.debug("inverse multiplex signal:\t"+beanName+"\tfor links types\t");
        links.stream().forEach(link -> logger.debug("\t link type"+link));
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.setTargetClass(AopUtils.getTargetClass(bean));
        proxyFactory.setTargetSource(new SingletonTargetSource(bean));
        proxyFactory.addAdvisor(new DefaultIntroductionAdvisor(new InverseMultiplexerIntroductionInterceptor(links)));
        proxyFactory.setOptimize(true);
        proxyFactory.setExposeProxy(true);
        return proxyFactory.getProxy();
    }

}
