/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.cop;

import org.springframework.beans.factory.config.BeanExpressionContext;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;

/**
 *
 * Spring value resolver shortcut util
 *
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 */
public final class ExpressionValueResolver {


    /*
     *
     *
     *
     * @param value the String expression - accepts SpEl
     *
     * @return the expression result as Spring Expression resolver
     *
     */
    public static Object evaluate(String value) {
        ConfigurableBeanFactory beanFactory = (ConfigurableBeanFactory)BeanUtil.getBeanFactory();
        return beanFactory.getBeanExpressionResolver()
                .evaluate(value, new BeanExpressionContext(beanFactory, null));
    }

}
