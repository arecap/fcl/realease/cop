/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.cop.annotation;

import org.arecap.contextualj.lang.annotation.ContextPointcut;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * Indicates that an annotated class is a "ContextOriented", that can be defined
 * as a weaving for pointcut IoC contained beans.
 *
 * Context Oriented Programming (COP) paradigm complements Spring Aspect Oriented Programming (AOP)
 * and Object Oriented Programming (OOP) by providing a crosscutting concerns way of thinking about program structure.
 * The key unit of modularity in OOP is the class, in AOP the unit of modularity is the aspect,
 * whereas in COP the unit of the modularity is the aspect and andContext oriented bean. "ContextOriented" beans enable
 * the modularization of concerns by the aspect and running environment of the andContext.
 * Aspects enable the modularization of concerns such as transaction management that cut across
 * multiple types and objects.
 *
 * <p>This annotation serves as a specialization of {@link Component @Component},
 * allowing for implementation classes to be autodetected through classpath scanning.
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@ContextPointcut
@Component
@Documented
public @interface ContextOriented {
}
