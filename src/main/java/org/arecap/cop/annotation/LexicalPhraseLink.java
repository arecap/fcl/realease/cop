/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.cop.annotation;

import org.arecap.contextualj.lang.annotation.expression.LexicalPhrase;
import org.arecap.contextualj.lang.weaving.PointcutLinkSpecification;
import org.arecap.cop.ExpressionValueResolver;
import org.springframework.core.annotation.AnnotatedElementUtils;

import java.lang.reflect.AnnotatedElement;
import java.util.Arrays;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 */

@ExpressionResolver
public class LexicalPhraseLink implements PointcutLinkSpecification, SpringLexicalExpressionResolver {


    @Override
    public boolean isInverseMultiplexerLink(AnnotatedElement signal, AnnotatedElement link) {
        LexicalPhrase lexicalPhrase = evaluate(link);
        return lexicalPhrase == null ? false :
                Arrays.stream(lexicalPhrase.value())
                        .filter(le -> resolveLexicalExpression(le.lex(),
                                ExpressionValueResolver.evaluate(le.value()),
                                ExpressionValueResolver.evaluate(le.valid()))).count() > 0;
    }

    @Override
    public LexicalPhrase evaluate(AnnotatedElement link) {
        return AnnotatedElementUtils.getMergedAnnotation(link, LexicalPhrase.class);
    }
}
