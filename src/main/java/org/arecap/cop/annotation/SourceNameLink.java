/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.cop.annotation;

import org.arecap.contextualj.lang.annotation.expression.SourceName;
import org.arecap.contextualj.lang.weaving.PointcutLinkSpecification;
import org.springframework.core.annotation.AnnotatedElementUtils;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 */

@ExpressionResolver
public class SourceNameLink implements PointcutLinkSpecification {


    @Override
    public boolean isInverseMultiplexerLink(AnnotatedElement signal, AnnotatedElement link) {
        SourceName sourceName = evaluate(link);
        return sourceName == null ? false :
                ((Method)signal).getName()
                        .equalsIgnoreCase(sourceName.value().trim().length() == 0 ? ((Method)link).getName() :
                                sourceName.value());
    }

    @Override
    public SourceName evaluate(AnnotatedElement link) {
        return AnnotatedElementUtils.getMergedAnnotation(link, SourceName.class);
    }

}
