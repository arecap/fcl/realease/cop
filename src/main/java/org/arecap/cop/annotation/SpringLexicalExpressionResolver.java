/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.cop.annotation;

import org.arecap.contextualj.lang.annotation.expression.LexicalExpression;
import org.springframework.util.Assert;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 */
public interface SpringLexicalExpressionResolver {


    default boolean resolveLexicalExpression(String lex, Object expressionValue, Object expressionValid) {
        switch (lex) {
            case LexicalExpression.IDENTITY:
                return expressionValue .equals(expressionValid);
            case LexicalExpression.BETWEEN:
                assertionLexNumberBetween(expressionValue, expressionValid);
                return (((Number)expressionValue).doubleValue() >= ((Number[])expressionValid)[0].doubleValue())
                        && (((Number)expressionValue).doubleValue() <= ((Number[])expressionValid)[1].doubleValue());
            case LexicalExpression.GT:
                assertionLexNumberComparator(expressionValue, expressionValid);
                return (((Number)expressionValue).doubleValue() > ((Number)expressionValid).doubleValue());
            case LexicalExpression.GTE:
                assertionLexNumberComparator(expressionValue, expressionValid);
                return  (((Number)expressionValue).doubleValue() >= ((Number)expressionValid).doubleValue());
            case LexicalExpression.LT:
                assertionLexNumberComparator(expressionValue, expressionValid);
                return  (((Number)expressionValue).doubleValue() < ((Number)expressionValid).doubleValue());
            case LexicalExpression.LTE:
                assertionLexNumberComparator(expressionValue, expressionValid);
                return (((Number)expressionValue).doubleValue() <= ((Number)expressionValid).doubleValue());
        }
        return false;
    }

    default void assertionLexNumberBetween(Object expressionValue, Object expressionValid) {
        Assert.isAssignable(Number.class, expressionValue.getClass(), "@ContextExpression lex BETWEEN value type not supported! " +
                "Value must be a valid Number assignable type!");
        Assert.isAssignable(Number[].class, expressionValid.getClass(), "@ContextExpression lex BETWEEN valid type not supported! " +
                "Valid must be a valid Number array assignable type!");
        Assert.isTrue(((Number[])expressionValid).length == 2, "Valid number array length not supported! " +
                "Current: " + ((Number[])expressionValid).length+" Expected: 2!");

    }

    default void assertionLexNumberComparator(Object expressionValue, Object expressionValid) {
        Assert.isAssignable(Number.class, expressionValue.getClass(), "@ContextExpression lex BETWEEN value type not supported! " +
                "Value must be a valid Number assignable type!");
        Assert.isAssignable(Number.class, expressionValid.getClass(), "@ContextExpression lex BETWEEN valid type not supported! " +
                "Valid must be a valid Number assignable type!");
    }

}
