/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.arecap.cop.proxy;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.arecap.contextualj.lang.annotation.ContextTarget;
import org.arecap.contextualj.lang.annotation.pointcut.*;
import org.arecap.contextualj.lang.weaving.InverseMultiplexer;
import org.arecap.cop.ApplicationContextLink;
import org.arecap.cop.BeanUtil;
import org.springframework.aop.support.DelegatingIntroductionInterceptor;
import org.springframework.core.MethodIntrospector;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 */
public class InverseMultiplexerIntroductionInterceptor extends DelegatingIntroductionInterceptor
        implements InverseMultiplexer<MethodInvocation, Object> {

    private static final long serialVersionUID = 0l;
    /** Logger available to subclasses */
    protected final Log logger = LogFactory.getLog(getClass());

    public static final ReflectionUtils.MethodFilter bidSelector = method -> AnnotatedElementUtils.getMergedAnnotation(method, Bid.class) != null;

    public static final ReflectionUtils.MethodFilter wrapSelector = method -> AnnotatedElementUtils.getMergedAnnotation(method, Wrap.class) != null;

    public static final ReflectionUtils.MethodFilter trackSelector = method -> AnnotatedElementUtils.getMergedAnnotation(method, Track.class) != null;

    public static final ReflectionUtils.MethodFilter finalBindingSelector = method -> AnnotatedElementUtils.getMergedAnnotation(method, FinalBinding.class) != null;

    public static final ReflectionUtils.MethodFilter handlingReturnSelector = method -> AnnotatedElementUtils.getMergedAnnotation(method, HandlingReturn.class) != null;

    public static final ReflectionUtils.MethodFilter handlingThrowingSelector = method -> AnnotatedElementUtils.getMergedAnnotation(method, HandlingThrowing.class) != null;

    public static final ReflectionUtils.FieldFilter contextTargetSelector = field -> AnnotatedElementUtils.getMergedAnnotation(field, ContextTarget.class) != null;

    private final List<Class<?>> links;

    public InverseMultiplexerIntroductionInterceptor(List<Class<?>> links) {
        this.links = links;
    }

    @Override
    protected Object doProceed(MethodInvocation mi) throws Throwable {
        logger.debug("inverse multiplexer of signal method:\t"+mi.getMethod().getName()+"\t of object instance type:\t"+mi.getThis().getClass());
        return inverseMultiplex(mi);
    }

    @Override
    public void bidMux(MethodInvocation methodInvocation) throws Throwable {
        for(Class<?> link: links) {
            for (Method bidMethod : getLinks(methodInvocation.getMethod(), MethodIntrospector.selectMethods(link, bidSelector).stream())) {
                new ContextOrientedMethodInvocation(getLink(methodInvocation.getThis(), link), bidMethod, resolveLinkArguments(bidMethod, methodInvocation)).proceed();

            }
        }
    }

    @Override
    public void returnMux(MethodInvocation methodInvocation, Object o) throws Throwable {
        for(Class<?> link: links) {
            for (Method handlingReturnMethod : getLinks(methodInvocation.getMethod(), MethodIntrospector.selectMethods(link, handlingReturnSelector).stream())) {
                new ContextOrientedMethodInvocation(getLink(methodInvocation.getThis(), link), handlingReturnMethod, resolveHandlingReturnLinkArguments(handlingReturnMethod, methodInvocation, o)).proceed();
            }
        }
    }

    @Override
    public void finalMux(MethodInvocation methodInvocation, Object o, Throwable throwable) {
        for(Class<?> link: links) {
            for (Method finalBidingMethod : getLinks(methodInvocation.getMethod(), MethodIntrospector.selectMethods(link, finalBindingSelector).stream())) {
                try {
                    new ContextOrientedMethodInvocation(getLink(methodInvocation.getThis(), link), finalBidingMethod, resolveFinalBindingLinkArguments(finalBidingMethod, methodInvocation, o, throwable)).proceed();
                } catch (Throwable throwable1) {
                    logger.warn("@link FinalBinding advices should not be throwable! Implemented not supported!", throwable1);
                }
            }
        }
    }

    @Override
    public void throwingMux(MethodInvocation methodInvocation, Throwable throwable) {
        for(Class<?> link: links) {
            for (Method handlingThrowingMethod : getLinks(methodInvocation.getMethod(), MethodIntrospector.selectMethods(link, handlingThrowingSelector).stream())) {
                try {
                    new ContextOrientedMethodInvocation(getLink(methodInvocation.getThis(), link), handlingThrowingMethod, resolveHandlingThrowingLinkArguments(handlingThrowingMethod, methodInvocation, throwable)).proceed();
                } catch (Throwable throwable1) {
                    logger.warn("@link HandlingThrowing advices should not be throwable! Implemented not supported!", throwable1);
                }
            }
        }
    }

    @Override
    public Object wrapTrackMux(MethodInvocation methodInvocation) throws Throwable {
        WrapMethodInvocation wrapMethodInvocation = new WrapMethodInvocation(methodInvocation);
        Set<MethodInvocation> trackMethodInvocation = new LinkedHashSet<>();
        for(Class<?> link: links) {
            for (Method wrapMethod : getLinks(methodInvocation.getMethod(), MethodIntrospector.selectMethods(link, wrapSelector).stream())) {
                wrapMethodInvocation.add(new ContextOrientedMethodInvocation(getLink(methodInvocation.getThis(), link), wrapMethod, resolveWrapLinkArguments(wrapMethod, methodInvocation)));

            }
            for(Method trackMethod :
                    getLinks(methodInvocation.getMethod(),
                            MethodIntrospector.selectMethods(link, trackSelector).stream())) {
                trackMethodInvocation
                        .add(new ContextOrientedMethodInvocation(getLink(methodInvocation.getThis(), link),
                                trackMethod, resolveLinkArguments(trackMethod, methodInvocation)));

            }
        }
        wrapMethodInvocation.add(new TrackMethodInvocation(methodInvocation, trackMethodInvocation));
        return wrapMethodInvocation.proceed();
    }

    private List<Method> getLinks(Method signal, Stream<Method> links) {
        return links.filter(link -> ApplicationContextLink.isLink(signal, link))
                .sorted(new AnnotationAwareOrderComparator()).collect(Collectors.toList());
    }

    private boolean hasNoArgs(Method linkMethod) {
        return linkMethod.getParameterTypes().length == 0;
    }

    private boolean isObjectTypeFirstParameter(Method adviceMethod) {
        return Object.class.isAssignableFrom(adviceMethod.getParameterTypes()[0]);
    }

    private boolean isThrowableTypeFirstParameter(Method adviceMethod) {
        return adviceMethod.getParameterTypes()[0].isAssignableFrom(Throwable.class);
    }

    private Object[] argsMatchBinding(Method linkMethod, MethodInvocation joinPoint) {
        if(linkMethod.getParameterTypes().length != joinPoint.getArguments().length) {
            throw new IllegalArgumentException();
        }
        for(Class<?> adviceMethodParameterType: linkMethod.getParameterTypes()) {
            for(Object methodInvocationArgument: joinPoint.getArguments()) {
                if(!adviceMethodParameterType.isAssignableFrom(methodInvocationArgument.getClass())) {
                    throw new IllegalArgumentException();
                }
            }
        }
        return joinPoint.getArguments();
    }

    private Object[] resolveLinkArguments(Method linkMethod, MethodInvocation joinPoint) {
        if(hasNoArgs(linkMethod)) {
            return new Object[0];
        }
        return argsMatchBinding(linkMethod, joinPoint);
    }

    private Object[] resolveFinalBindingLinkArguments(Method linkMethod, MethodInvocation joinPoint, Object returnObj, Throwable throwable) {
        if(hasNoArgs(linkMethod)) {
            return new Object[0];
        }
        if(linkMethod.getParameterTypes().length == 1) {
            if(isThrowableTypeFirstParameter(linkMethod)) {
                return new Object[] { throwable };
            }
            return new Object[] { returnObj };
        }
        if(linkMethod.getParameterTypes().length != 2) {
            throw new IllegalArgumentException();
        }
        if(!isObjectTypeFirstParameter(linkMethod) &&
                !linkMethod.getParameterTypes()[1].isAssignableFrom(Throwable.class)) {
            throw new IllegalArgumentException();
        }
        return new Object[] { returnObj, throwable };
    }

    private Object[] resolveHandlingReturnLinkArguments(Method linkMethod, MethodInvocation joinPoint, Object returnObj) {
        if(hasNoArgs(linkMethod)) {
            return new Object[0];
        }
        if(linkMethod.getParameterTypes().length != 1) {
            throw new IllegalArgumentException();
        }
        if(!isObjectTypeFirstParameter(linkMethod)) {
            throw new IllegalArgumentException();
        }
        return new Object[] { returnObj };
    }

    private Object[] resolveHandlingThrowingLinkArguments(Method linkMethod, MethodInvocation joinPoint, Throwable throwable) {
        if(hasNoArgs(linkMethod)) {
            return new Object[0];
        }
        if(linkMethod.getParameterTypes().length != 1) {
            throw new IllegalArgumentException();
        }
        if(!isThrowableTypeFirstParameter(linkMethod)) {
            throw new IllegalArgumentException();
        }
        return new Object[] { throwable };
    }

    public Object[] resolveWrapLinkArguments(Method linkMethod, MethodInvocation joinPoint) {
        if(linkMethod.getParameterTypes().length != 1) {
            throw new IllegalArgumentException();
        }
        if(!MethodInvocation.class.isAssignableFrom(linkMethod.getParameterTypes()[0])) {
            throw new IllegalArgumentException();
        }
        return new Object[] { joinPoint };
    }

    private Object getLink(Object signal, Class<?> linkType) {
        Object link = BeanUtil.getBean(linkType);
        ReflectionUtils.doWithFields(linkType, field -> {
            if(field.getType().isAssignableFrom(signal.getClass())) {
                boolean acc = field.isAccessible();
                field.setAccessible(true);
                field.set(link, signal);
                field.setAccessible(acc);
            }else {
                logger.warn("ContextOriented type " + linkType + " declared error! @ContextTarget field type mismatch.");

            }
        }, contextTargetSelector);
        return link;
    }

}
