/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.arecap.cop.proxy;

import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Method;
import java.util.Set;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 */
public class TrackMethodInvocation implements MethodInvocation {

    private final MethodInvocation beanMethodInvocation;

    private final Set<MethodInvocation> proceedJoinPoints;

    public TrackMethodInvocation(MethodInvocation beanMethodInvocation, Set<MethodInvocation> proceedJoinPoints) {

        this.beanMethodInvocation = beanMethodInvocation;
        this.proceedJoinPoints = proceedJoinPoints;
    }


    @Override
    public Method getMethod() {
        return beanMethodInvocation.getMethod();
    }

    @Override
    public Object[] getArguments() {
        return beanMethodInvocation.getArguments();
    }

    @Override
    public Object proceed() throws Throwable {
        Object returnMulticast = beanMethodInvocation.proceed();
        for(MethodInvocation contextOrientedMethodInvocation: proceedJoinPoints) {
            Object returnTrack = contextOrientedMethodInvocation.proceed();
            if(returnTrack != null && returnTrack.getClass().isAssignableFrom(returnMulticast.getClass()))
                returnMulticast = returnTrack;
        }
        return returnMulticast;
    }

    @Override
    public Object getThis() {
        return beanMethodInvocation.getThis();
    }

    @Override
    public AccessibleObject getStaticPart() {
        return beanMethodInvocation.getStaticPart();
    }
}

