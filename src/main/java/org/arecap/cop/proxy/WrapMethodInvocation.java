/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.arecap.cop.proxy;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.ProxyMethodInvocation;
import org.springframework.util.Assert;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Method;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 */
public class WrapMethodInvocation implements MethodInvocation {

    private final Set<MethodInvocation> proceedMethodInvocationChain = new LinkedHashSet<>();

    private final MethodInvocation jointPoint;

    public WrapMethodInvocation(MethodInvocation jointPoint) {
        this.jointPoint = jointPoint;
    }

    public void add(MethodInvocation proceedMethodInvocation) {
        proceedMethodInvocationChain.add(proceedMethodInvocation);
    }

    public void addAll(Set<MethodInvocation> proceedMethodInvocations) {
        proceedMethodInvocationChain.addAll(proceedMethodInvocations);
    }

    public void setArguments(Object... arguments) {
        Assert.isAssignable(ProxyMethodInvocation.class, jointPoint.getClass());
        ((ProxyMethodInvocation) jointPoint).setArguments(arguments);
    }

    @Override
    public Method getMethod() {
        return this.jointPoint.getMethod();
    }

    @Override
    public Object[] getArguments() {
        return this.jointPoint.getArguments();
    }

    @Override
    public Object proceed() throws Throwable {
        MethodInvocation methodInvocation = proceedMethodInvocationChain.iterator().next();
        proceedMethodInvocationChain.remove(methodInvocation);
        if(proceedMethodInvocationChain.isEmpty()) {
            return methodInvocation.proceed();
        }
        return
                new ContextOrientedMethodInvocation(methodInvocation.getThis(),
                        methodInvocation.getMethod(),
                        new Object[]{this}).proceed();
    }

    @Override
    public Object getThis() {
        return this.jointPoint.getThis();
    }

    @Override
    public AccessibleObject getStaticPart() {
        return this.jointPoint.getStaticPart();
    }

}

